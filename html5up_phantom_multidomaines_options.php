<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

//garder les urls courte pour les secteurs et non avoir l’url complète
if (!defined("_SECTEUR_URL")) {
	define('_SECTEUR_URL', '1');
}

//URLS ARBORESCENTES →à activer dans la config SPIP sous
// /ecrire/?exec=configurer_urls
// URL article avec seulement le 1er niveau de rubrique
// URL mot avec son groupe dans l'url
$GLOBALS['url_arbo_parents'] = array(
	'rubrique'=> array(),
	'mot'=>array('id_groupe','groupes_mot'),
);

//on évite d'inscrire dans l'url l'objet demandé
$GLOBALS['url_arbo_types']=array(
 'article'=>'', // pas de type pour les articles
 'groupes_mot'=>'',
 'mot'=>''
);