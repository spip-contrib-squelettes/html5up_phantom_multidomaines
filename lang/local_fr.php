<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'entree_adresse_email' => 'Email',
	/* 'envoyer_message' => 'envoi_message', hidden css */ 
	'form_prop_sujet' => 'Sujet',
	'form_prop_envoyer' => 'Envoi',
	'info_texte_message' => 'Message',
);
